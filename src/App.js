
import  { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router,Routes,Route } from 'react-router-dom'
import './App.css';
import NavBar from './components/NavBar';
import Admin from './pages/Admin';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';

import Product from './pages/Product';
import ViewProduct from './pages/ViewProduct';
import Checkout from './pages/Checkout';
import UsersOrder from './pages/UsersOrder'
import {UserProvider} from './UserContext';
function App() {
  const [user, setUser] = useState({
    id:null,
    isAdmin:null
    
  })

  const unsetUser = () => {
    localStorage.clear()
  }
  const token = localStorage.getItem('token')
  useEffect(()=>{
    if(token !== null){
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }  
        })
        .then(res=>res.json())
        .then(data=>{  
           if(typeof data._id !== "undefined" ){
            setUser({
              id:data._id,
              isAdmin: data.isAdmin
            })
           }else{
            setUser({
              id:null,
              isAdmin: null
              })
          }
        })
      }
  },[token])
  
  return (
    <UserProvider value={{user,setUser,unsetUser}}>
    <Router>
      <Container>
        <NavBar/>
        <Routes>
          (user.isAdmin) ?
          <Route path='/admin' element={<Admin/>}/>
          <Route path='/addProd' element={<AddProduct/>}/>
          <Route path='/products/update/:productId' element={<EditProduct/>}/>
          :
          <Route path='/' element={<Home/>}/>
          <Route path='/products' element={<Product/>}/>
          <Route path='/products/view/:id' element={<ViewProduct/>}/>
          <Route path='/orders/checkout/:productId' element={<Checkout/>}/>
          <Route path='/orders' element={<UsersOrder/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Logout/>}/>  
          <Route path='/register' element={<Register/>}/>
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
