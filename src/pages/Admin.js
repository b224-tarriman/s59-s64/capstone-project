import { Navigate } from 'react-router-dom'
import { Button } from 'react-bootstrap'
import { useContext } from 'react'
import Product from './Product'
import UserContext from '../UserContext'
import { Link } from 'react-router-dom'
export default function Admin(){
   const {user} = useContext(UserContext)

  return (
    <>
    {

      (user.id === null) ?
        <Navigate to="/"/>
        :
        <>
        <h1 className='text-center'>Admin</h1>
        <Button id='prodBtn' as={Link} to="/addProd">Add New Product</Button>
        <Product/>
        </>
    }

    </>
  )
}

