import { useState, useContext } from 'react'
import { Container,Row,Col,Button,Card } from 'react-bootstrap'
import { useParams,useNavigate,Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function ViewProduct(){
    const {user} = useContext(UserContext)
    const {id} = useParams()
    const navigate = useNavigate()

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
        if(user.id === null){
            fetch(`${process.env.REACT_APP_API_URL}/products/nonuser/${id}`).then(res=>res.json())
            .then(data=>{
                console.log(data)
                
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setStock(data.stock)
            })
        }else{
            fetch(`${process.env.REACT_APP_API_URL}/products/view/${id}`,{
                headers:{
                    Authorization:`Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res=>res.json())
            .then(data=>{
                console.log(data)
                
                setName(data.name)
                setDescription(data.description)
                setPrice(data.price)
                setStock(data.stock)
            })
        }
        
   
    

    return (
       
        <Container>
        <Row className='mt-3 mb-3'>
           <Col lg={{span:6,offset:3}}>
               <Card>
                   <Card.Body className='text-center'>
                       <Card.Title>{name}</Card.Title>
                       <Card.Subtitle>Description:</Card.Subtitle>
                       <Card.Text>
                       {description}
                       </Card.Text>
                       <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                       <Card.Text>
                       PhP {price}
                       </Card.Text>
                       <Card.Subtitle>Available:</Card.Subtitle>
                       <Card.Text>{stock}</Card.Text>
                   </Card.Body>
                   {
                       (user.id !== null) ?
                       <Button variant='primary' as={Link} to={`/orders/checkout/${id}`}>Checkout</Button>
                       :
                       <Button className='btn btn-danger' as={Link} to="/login">Log in to buy</Button>

                       
                   }
               </Card>                
           </Col>
       </Row>
   </Container>
      
    )
}