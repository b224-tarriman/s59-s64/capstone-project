import { useState,useEffect,useContext } from 'react'
import { Form,Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){
    const {user} = useContext(UserContext)
    // console.log(user.email)
    const navigate = useNavigate();
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [fName, setFirstName] = useState('')
    const [lName, setLastName] = useState('')
    const [mobileNum, setMobileNumber] = useState('')
    // State to determine whether the submit button is enabled or not.
    const [isActive, setIsActive]= useState(false)

    // console.log(email)
    // console.log(password1)
    // console.log(password2)
    

    // Function to simulate user registration
    function registerUser(e){
        e.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
            method:'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: fName,
                lastName: lName,
                email: email,
                password:password2,
                mobileNo:mobileNum
            })
        })
        .then(res=> res.json())
        .then(data=>{
            console.log(data)
            if(data){
                Swal.fire({
                    title:"Registered Succesfully",
                    icon:"success",
                    text:"Enjoy your stay",
                    timer:2000
                })

                navigate("/login")
                
            }else{
                Swal.fire({
                    title:"Email exists",
                    icon:"error",
                    text:"Please use a different email.",
                    timer:2000
                })
            }
        })
       
        // Clear the input fields and states
        setFirstName("")
        setLastName("")
        setMobileNumber("")
        setEmail("")
        setPassword1("")
        setPassword2("")
    }

    useEffect(()=>{
        if((email !== "" && password1 !== "" && password2 !== "")&&(password1 === password2) && fName !== "" && lName !== "" && mobileNum.length === 11){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[email,password1,password2,fName,lName,mobileNum])

    return(
        (user.id !== null) ?
        <Navigate to="/login"/>
        :
        <Form onSubmit={e=>registerUser(e)}>
        <h1 className='text-center'>Registration</h1>
            <Form.Group className="mb-3" controlId="firstName" >
                <Form.Label>First Name</Form.Label>
                <Form.Control
                    type="string"
                    placeholder="Enter First Name"
                    value={fName}
                    onChange={e=> setFirstName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                    type="string"
                    placeholder="Enter Last Name"
                    value={lName}
                    onChange={e=> setLastName(e.target.value)}
                    required
                />
            </Form.Group>
          <Form.Group className="mb-3" controlId="email" >
            <Form.Label>Email address</Form.Label>
            <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={e=> setEmail(e.target.value)}
                required
            />
            <Form.Text>We'll never share your email with anyone else.</Form.Text>
          </Form.Group>
          <Form.Group className="mb-3" controlId="mobileNum" >
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                    type="tel"
                    maxLength="11"
                    placeholder="Enter Mobile Number"
                    value={mobileNum}
                    onChange={e=> setMobileNumber(e.target.value)}
                    required
                />
            </Form.Group>
          <Form.Group className="mb-3" controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password1}
                onChange={e=> setPassword1(e.target.value)}
                required
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="password2">
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Confirm Password" 
                value={password2}
                onChange={e=> setPassword2(e.target.value)}
                required
            />
          </Form.Group>
          {
            isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="primary" type="submit" disabled >
                Submit
                </Button>
          }
        </Form>
    )
}