import { useState,useEffect, useContext } from 'react'
import { Container,Card,Button,Row,Col } from 'react-bootstrap'
import { useParams,useNavigate,Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default  function Checkout(){
    const {user} =  useContext(UserContext)
    const navigate = useNavigate()
    const {productId} = useParams()
    const [quantity, setQuantity] = useState(1)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    
    if(user.id === null){
        fetch(`${process.env.REACT_APP_API_URL}/products/nonuser/${productId}`)
        .then(res=>res.json())
        .then(data=>{
            console.log(data)
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setStock(data.stock)
        })
    }else{
        fetch(`${process.env.REACT_APP_API_URL}/products/view/${productId}`,{
            headers:{
                Authorization:`Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res=>res.json())
        .then(data=>{
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setStock(data.stock)
        })
    }
        
        
        function checkout(productId){
            fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,{
                method:"POST",
                headers:{
                    'Content-Type':'application/json',
                    Authorization:`Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                   products:{
                        productId: productId,
                        quantity: quantity
                   }
                })
            }).then(res=>res.json()).then(data=>{
                console.log(data)
                
                if (data) {
                    
                    Swal.fire({
                        title:"Order on your way!",
                        icon:"success",
                        text:"You have successfully ordered the product"
                    })
                    navigate('/orders')
                }else{
                    Swal.fire({
                        title:"Something went wrong!",
                        icon:"error",
                        text:"Try again later."
                    })
                    
                }
            })
        }


        useEffect(()=>{
            const ele = document.querySelector('.buble');
          if (ele) {
            ele.style.left = `${Number(quantity / 4)}px`;
          }
        })


    return(
        
        <Container>
        <Row className='mt-3 mb-3'>
           <Col lg={{span:6,offset:3}}>
               <Card>
                   <Card.Body className='text-center'>
                       <Card.Title>{name}</Card.Title>
                       <Card.Subtitle>Description:</Card.Subtitle>
                       <Card.Text>
                       {description}
                       </Card.Text>
                       <Card.Subtitle className="mb-2">Price:</Card.Subtitle>
                       <Card.Text>
                       PhP {price}
                       </Card.Text>
                   </Card.Body>
                   <div className='buble text-center'>
                            Quantity: {quantity}
                    </div>
                   <div className='slider-parent text-center'>
                        <input 
                        type="range"
                        min="1" 
                        max={stock}
                        value={quantity}
                        onChange={({ target: { value: radius } }) => {setQuantity(radius)}}
                        />
                        
                   </div>
                        
                   {
                       (user.id !== null) ?
                       <Button variant='primary'  onClick={()=>checkout(productId)} >Checkout</Button>
                       :
                       <Button className='btn btn-danger' as={Link} to="/login">Log in to buy</Button>

                       
                   }
               </Card>                
           </Col>
       </Row>
   </Container>
    )
}
