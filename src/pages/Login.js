import { useState,useEffect,useContext } from 'react'
import { Form,Button } from 'react-bootstrap'
import { Navigate,useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(){
    const {user, setUser} = useContext(UserContext)
    const navigate = useNavigate()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive]= useState(false)

    // Function to simulate user registration
    function authenticate(e){
        e.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
            method:'POST',
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res=> res.json())
        .then(data=>{
            // We will receive either a token or a false response
            
            if(typeof data.access !== "undefined"){
                localStorage.setItem('token',data.access)
                retrieveUserDetails(data.access)
                
                Swal.fire({
                    title:"Login Succesfully",
                    icon:"success",
                    text:"Welcome to Zuitt",
                })
                navigate('/')
                
                
            }else{
                Swal.fire({
                title:"Authentication Failed",
                icon:"error",
                text:"Please, check your login details and try again.",
                timer:2000
            })
        }
        })
        setEmail("")
        setPassword("")
    }

    

    const retrieveUserDetails = (token) =>{
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers:{
                Authorization: `Bearer ${token}`
            }  
        }).then(res=>res.json()).then(data=>{
            setUser({
                id:data._id,
                isAdmin:data.isAdmin
            })
            if (data.isAdmin){
                Swal.fire({
                    title:"Login Succesfully",
                    icon:"success",
                    text:"Welcome,Admin!",
                })
                navigate('/admin')
            }
            
            
        })
    }
    
    useEffect(()=>{
        if(email !== "" && password !== "" ){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[email,password])

    return(

         (user.id !== null) ?
            <Navigate to="/"/>
            : 
            <Form onSubmit={e=>authenticate(e)}>
                <h1 className='text-center'>LOGIN</h1>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={e=> setEmail(e.target.value)}
                    required
                />
                </Form.Group>
                <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e=> setPassword(e.target.value)}
                    required
                />
                </Form.Group>
                {
                    isActive ?
                        <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                        </Button>
                        :
                        <Button variant="primary" type="submit" disabled >
                        Submit
                        </Button>
                }
            </Form>
      
    
    )
}