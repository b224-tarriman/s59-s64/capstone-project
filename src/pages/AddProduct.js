import { useState,useEffect,useContext } from 'react'
import { Form,Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

const AddProduct = () => {
    const {user} = useContext(UserContext)
    const navigate = useNavigate()
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')


    function addProd(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`,
        {
            method:'POST',
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        }).then(res=>res.json()).then(data=>{
            console.log(data)
            if(data){
                Swal.fire({
                    title:"Product added",
                    icon:"success",
                    timer:2000
                })
                navigate('/admin')
            }else{
                Swal.fire({
                    title:"Something went wrong!",
                    icon:"error",
                    text:"Try again later.",
                    timer:2000
                })
                navigate('/admin')
            }
        })
    }


    const [isActive, setIsActive] = useState(false)
    useEffect(()=>{
        if((name !== "" && description !== "" && price !== "")&&stock !== ""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[name,description,price,stock])

  return (
    (user.id === null) ?
        <Navigate to="/"/>
    :
    <Form onSubmit={e=>addProd(e)}>
    <h1 className='text-center'>Add Product</h1>
        <Form.Group className="mb-3" controlId="firstName" >
            <Form.Label>Product Name</Form.Label>
            <Form.Control
                type="string"
                placeholder="Enter Product Name"
                value={name}
                onChange={e=> setName(e.target.value)}
                required
            />
        </Form.Group>
        <Form.Group className="mb-3" controlId="lastName">
            <Form.Label>Description</Form.Label>
            <Form.Control
                type="string"
                placeholder="Enter Description"
                value={description}
                onChange={e=> setDescription(e.target.value)}
                required
            />
        </Form.Group>
      <Form.Group className="mb-3" controlId="email" >
        <Form.Label>Price</Form.Label>
        <Form.Control
            type="string"
            placeholder="Enter Price"
            value={price}
            onChange={e=> setPrice(e.target.value)}
            required
        />
      
      </Form.Group>
      <Form.Group className="mb-3" controlId="mobileNum" >
            <Form.Label>Stock</Form.Label>
            <Form.Control
                type="number"
                placeholder="Enter Stock Available"
                value={stock}
                onChange={e=> setStock(e.target.value)}
                required
            />
        </Form.Group>
      
      {
        isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
            Submit
            </Button>
            :
            <Button variant="primary" type="submit" disabled >
            Submit
            </Button>
      }
    </Form>
)
}

export default AddProduct
