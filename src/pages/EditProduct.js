import { useState,useEffect, useContext } from 'react'
import { Form,Button } from 'react-bootstrap'
import { useParams,useNavigate,Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function EditProduct(){
    const {user} = useContext(UserContext)
    const {productId} = useParams()
    const navigate = useNavigate()
    const [isActive, setIsActive] = useState(true)
    


    const update= (productId) =>{
        
        fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`,{
            method:"PUT",
            headers:{
                'Content-Type':'application/json',
                Authorization:`Bearer ${localStorage.getItem('token')}`
            },
            body:JSON.stringify({
                name: name,
                description: description,
                price: price,
                stock: stock
            })
        })
            .then(res=>res.json())
            .then(data=>{
                if(data){
                    Swal.fire({
                        title:"Updated successfully",
                        icon:"success",
                        text:"You have successfully updated the product"
                    })
                    navigate('/admin')
                }else{
                    Swal.fire({
                        title:"Failed update attempt",
                        icon:"error",
                        text:"Try again"
                    })
                }
            })
        
    }
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [stock, setStock] = useState('')
    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/view/${productId}`,{
            headers:{
                Authorization:`Bearer ${localStorage.getItem('token')}`
            }
        }).then(res=>res.json()).then(data=>{
            console.log(data)
            
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
            setStock(data.stock)
        })
    },[productId])
    useEffect(()=>{
        if((name !== "" && description !== "" && price !== "")&&stock !== ""){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    },[name,description,price,stock])

    return (
       
        <Form >
            <h1 className='text-center'>Update</h1>
            <Form.Group className="mb-3"  >
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                    type="string"
                    placeholder="Enter Product Name"
                    value={name}
                    onChange={e=> setName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group className="mb-3" >
                <Form.Label>Description</Form.Label>
                <Form.Control
                    type="string"
                    placeholder="Enter Description"
                    value={description}
                    onChange={e=> setDescription(e.target.value)}
                    required
                    />
            </Form.Group>
            <Form.Group className="mb-3" >
                <Form.Label>Price</Form.Label>
                <Form.Control
                    type="string"
                    placeholder="Enter Price"
                    value={price}
                    onChange={e=> setPrice(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group className="mb-3"  >
                <Form.Label>Stock</Form.Label>
                <Form.Control
                    type="number"
                    placeholder="Enter Stock Available"
                    value={stock}
                    onChange={e=> setStock(e.target.value)}
                    required
                />
            </Form.Group>
            {
                isActive ?
                <Button variant="primary"  id="submitBtn" onClick={()=>update(productId)}>Update</Button>
                :
                <Button variant="primary" type="submit" disabled >Update</Button>
            }
            
     
    </Form>
      
    )
}