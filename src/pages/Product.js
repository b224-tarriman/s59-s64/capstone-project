import {useContext,useState, useEffect} from 'react'
import UserContext from '../UserContext'
import TableProducts from '../components/TableProducts'
import { Table } from 'react-bootstrap'
export default function Product (){
    const {user} = useContext(UserContext)
    const [products, setProducts] = useState([])
    let token = localStorage.getItem('token')
    
    if(token === null){
            fetch(`${process.env.REACT_APP_API_URL}/products/`)
            .then(res=> res.json())
            .then(data =>{
                
                setProducts(data.map(product => {
                    return(
                        <TableProducts key={product._id} productProp={product}/>
                    )
                    
                }))
                
            }) 
    }else{
            fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res=> res.json())
            .then(data =>{
                setProducts(data.map(product => {
                    return(
                        <TableProducts key={product._id} productProp={product}/>
                    )
                }))
            }) 
        
    }
    
  return (
    <>
    {
    (user.isAdmin) ?
    <Table striped bordered hover className='mt-2'>
        <thead >
                <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Availability</th>
                <th>Actions</th>
                </tr>
        </thead>
        {products}
    </Table>
    :
    <Table striped bordered hover className='mt-2'>
        <thead >
                <tr>
                <th>Name</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Actions</th>
                </tr>
        </thead>
        {products}
    </Table>
    }
    </>
  )
}


