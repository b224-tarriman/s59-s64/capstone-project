import {useContext,useState} from 'react'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import TableOrders from '../components/TableOrders'
import { Table,Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
export default function Orders (){
    const {user} = useContext(UserContext)
    const [order, setOrders] = useState([])
    let token = localStorage.getItem('token')
    
    
           
    
            fetch(`${process.env.REACT_APP_API_URL}/orders/userOrders`,{
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res=> res.json())
            .then(data =>{
                
                setOrders(data.map(order => {
                    return(
                        <TableOrders key={order._id} orderProp={order}/>
                    )
                }))
            }) 
        
    
    
  return (
    <>
    {
    (user.isAdmin || token === null) ?
    <Navigate to="/"/>
    :
    <Table striped bordered hover className='mt-2'>
        <thead >
                <tr>
                <th>Product ID</th>
                <th>Quantity</th>
                <th>Total</th>
                
                </tr>
        </thead>
        {order}
        <Button as={Link} to ={`/products`} className='mx-md-2 mt-2'>Back</Button>
    </Table>
    }
    </>
  )
}


 