
import { useState,useContext } from 'react'
import UserContext from '../UserContext'

import { Navigate } from 'react-router-dom'
export default function Products({orderProp}){
    const {user} = useContext(UserContext)
    const [i, setI] = useState(0)
    const [prodId,setProdId] = useState("")
    const [prodQuantity, setProdQuantity] = useState("")
    
        if (orderProp.products.length !== i){
            const id = orderProp.products[i].productId
            const quantity = orderProp.products[i].quantity
            setProdId(id)
            setProdQuantity(quantity)
            setI(i+1)
        }
    
   
   
    
    return(
    <> 
    {
        (user.id === orderProp.userId) ?
        <>
            <tbody>
            <tr>
            <td>{prodId}</td>
            <td>{prodQuantity}</td>
            <td>{orderProp.totalAmount}</td>
            </tr>
            </tbody>
            
        </>
        :
        <Navigate to="/products"/>
    }
        
        
    </>
    )
}
