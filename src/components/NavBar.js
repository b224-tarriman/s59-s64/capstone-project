import { useContext } from 'react'
import { Navbar,Nav,Container } from 'react-bootstrap' 
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'


export default function AppNavBar(){

  const {user} = useContext(UserContext)
//   console.log(user)
  

    return (
         
          <>
            <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand as={Link} to="/"><img src="https://i.ibb.co/F3b4d09/Untitled-design.png" alt="Untitled-design" border="0"/>
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                {
                  (user.isAdmin ) ?
                  <Nav.Link as={Link} to="/admin">Admin Dashboard</Nav.Link>
                  :
                  <>
                  <Nav.Link as={Link} to="/">Home</Nav.Link>
                  <Nav.Link as={Link} to="/products">Product</Nav.Link>
                  </>
                }
                  {
                    (user.id !== null) ?
                    <>
                        <Nav.Link as={Link} to="/orders">Orders</Nav.Link>
                        <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                    </>
                      :
                      <>
                        <Nav.Link as={Link} to="/login">Login</Nav.Link>
                        <Nav.Link as={Link} to="/register">Register</Nav.Link>
                      </>
                  }
                 
                </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </>
                
                
    )
}