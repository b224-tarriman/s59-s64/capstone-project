import { Link} from 'react-router-dom'
import { useState,useEffect,useContext } from 'react'
import UserContext from '../UserContext'
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
export default function Products({productProp}){
    const {user} = useContext(UserContext)
    const {_id,name,description,price,stock,isActive} = productProp
    const [active, setIsActive] = useState("Available")
    
    const changeState = (id)=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`,{
            method:"PUT",
            headers:{
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res=>res.json()).then(data=>{
            
            if(data && isActive === true){
                Swal.fire({
                    title:"Archived Product",
                    icon:"success",
                    text:"You have successfully archived the product"
                })
            }else{
                Swal.fire({
                    title:"Activated Product",
                    icon:"success",
                    text:"You have successfully activated the product"
                })
            }
        })
    }
    useEffect(()=>{
        if(isActive === true){
            setIsActive("Available")
        }else{
            setIsActive("Unavailable")
        }
    },[isActive])

    
    return(
    <> 
        {
            (user.isAdmin) ?
            <tbody>
            <tr>
            <td>{name}</td>
            <td>{description}</td>
            <td>{price}</td>
            <td>{stock}</td>
            <td>{active}</td>
            <td><Button as={Link} to ={`/products/update/${_id}`} className='mx-md-2'>Update</Button>
            {
            (isActive) ?
            <Button variant='danger' className='mx-lg-2 mx-md-2' onClick={()=>changeState(_id)}>Disable</Button>
            :
            <Button variant='success' className='mx-lg-2 mx-md-2'onClick={()=>changeState(_id)}>Enable</Button>
            }
            </td>
            </tr>
            </tbody>

            :

            <tbody>
            <tr>
            <td>{name}</td>
            <td>{description}</td>
            <td>{price}</td>
            <td>{stock}</td>
            <td><Button as={Link} to ={`/products/view/${_id}`} className='mx-md-2'>Details</Button>
            
            <Button variant='success' className='mx-lg-2 mx-md-2' as={Link} to ={`/orders/checkout/${_id}`}>Checkout</Button>
            </td>
            </tr>
        </tbody>
        }
        
        
    </>
    )
}
